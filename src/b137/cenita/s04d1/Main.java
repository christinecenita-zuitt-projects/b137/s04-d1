package b137.cenita.s04d1;

public class
Main {

    public static void main(String[] args) {
        Car firstCar = new Car();

        firstCar.setName("Ertiga");
        firstCar.setBrand("Suzuki");
        firstCar.setYearOfMake(2000);

        firstCar.drive();

        Car secondCar = new Car("Avanza", "Toyota", 2005);

        secondCar.drive();
    }
}
